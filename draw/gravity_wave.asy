//==========================================================
//
//  Illustration of Wall Condensation
//
//==========================================================


/*==========================================================

Format Setup

==========================================================*/
import fontsize;
settings.outformat="pdf";
defaultpen(fontsize(2));
defaultpen(linewidth(0.03));
unitsize(1cm);

/*==========================================================

Preambles

==========================================================*/
import patterns;
import graph;
add("hatch",hatch(3mm));

/*==========================================================

Variable Declaration

==========================================================*/

real L=1;
real h=1.05;
real dh=0.1;
real dh2=0.05;
/*==========================================================

Draw

==========================================================*/

path p1=(0,0)--(0,h);

real f (real x) {
return 1+0.05*cos(2.*3.1415926*x/1.);
}
path p2=graph(f,0,1);
path p3=(L,h)--(L,0);

path p4=(L,h)--(L,h+dh)--(0,h+dh);

path lower=p1--p2--p3--cycle;
fill(lower,green);

draw((0,0)--(0,h+dh)--(L,h+dh)--(L,0)--cycle);
draw(p2,red);

Label lx=Label("$\mathrm{L=1m}$",position=MidPoint);
draw((0,-dh2)--(L,-dh2),arrow=Arrows,L=lx);

Label ly=Label("$\mathrm{H=1.15m}$",position=MidPoint);
draw((L+dh2,0)--(L+dh2,h+dh),arrow=Arrows,L=ly);

label("$\mathrm{y=1+0.05\cos(2\pi x)}$",(L/2,1),align=2.8S);
label("water",(L/2,0.5),align=S);
// y axis
//Label y=Label("$y$",position=EndPoint);
//draw((l_w,0)--(l_w,3),arrow=EndArrow,L=y);

// Temperature Profile
// real f (real x) {
// return 0.2/(4+dx-x);
// }
// path t_profile=graph(f,1,4+dx/2);
// draw(t_profile,red);

// liquid sublayer
//path liquid_sublayer=(x_1_1,0)--(x_1_1-dx,dx/4)--(x_1_1-dx,0)--cycle;
//fill(liquid_sublayer,green);
//path liquid_sublayer2=(x_1_3,0)--(x_1_3+dx,dx/4)--(x_1_3+dx,0)--cycle;
//fill(liquid_sublayer2,green);

// Wall with Cavities
//Label T=Label("$T_l$",position=BeginPoint);
//draw((0,0)--(x_1_1,0)--(x_1_2,-dh)--(x_1_3,0)--(l_w,0));
//// Bubble
//path bubble=(x_1_1,0)--(x_1_1-dx,dx/4){-dx,dx/4}..(x_1_2,2.){right}..{-dx,-dx/4}(x_1_3+dx,dx/4)--(x_1_3,0);
//draw(bubble,blue);
//
//label("Liquid Sublayer",(x_1_3+2,0.5),align=E);
//draw((x_1_3+2,0.5)--(x_1_3+dx,0.1),arrow=Arrow);
//
//// Saturation Line
//// draw((3.5,0)--(3.5,2),dashed);
//// label("$T_{sat}$",(3.5,0),align=S);
















