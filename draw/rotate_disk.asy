//==========================================================
//
//  Illustration of Wall Condensation
//
//==========================================================


/*==========================================================

Format Setup

==========================================================*/
import fontsize;
settings.outformat="pdf";
defaultpen(fontsize(2));
defaultpen(linewidth(0.03));
unitsize(1cm);

/*==========================================================

Preambles

==========================================================*/
import patterns;
import graph;
add("hatch",hatch(3mm));

/*==========================================================

Variable Declaration

==========================================================*/

real L=4;
real dh2=0.05;
/*==========================================================

Draw

==========================================================*/
real x0=1,x1=3,y0=1.5,y1=3.5;

path p1=(x0,y0)--(x1,y0)--(x1,y1)--(x0,y1)--cycle;
path p2=circle((2,2.75),0.5);
path p3=box((1.94,2.25361305),(2.06,2.75361305));
path p4=box((1.94,2.24),(2.06,2.75361305));

filldraw(p2,green,black);
draw(p1);
filldraw(p3,white,black);
fill(p4,white);


Label lx=Label("$\mathrm{L=4m}$",position=MidPoint);
draw((x0,y0-dh2)--(x1,y0-dh2),arrow=Arrows,L=lx);

Label ly=Label("$\mathrm{H=4m}$",position=MidPoint);
draw((x1+dh2,y0)--(x1+dh2,y1),arrow=Arrows,L=ly);

Label ld=Label("$\mathrm{D=1m}$",position=MidPoint,align=N);
draw((1.5,3.3)--(2.5,3.3),arrow=Arrows,L=ld);

Label lw=Label("$\mathrm{w=0.12m}$",position=MidPoint,align=S);
draw((1.94,2.2)--(2.06,2.2),arrow=Arrows,L=lw);


Label lh=Label("$\mathrm{h=0.5m}$",position=MidPoint,align=E);
draw((2.6,2.25)--(2.6,2.75),arrow=Arrows,L=lh);

dot((2,2.65));
label("(2,2.75)",(2,2.65),align=3E);

//label("$\mathrm{y=1+0.05\cos(2\pi x)}$",(L/2,1),align=2.8S);
//label("water",(L/2,0.5),align=S);
// y axis
//Label y=Label("$y$",position=EndPoint);
//draw((l_w,0)--(l_w,3),arrow=EndArrow,L=y);

// Temperature Profile
// real f (real x) {
// return 0.2/(4+dx-x);
// }
// path t_profile=graph(f,1,4+dx/2);
// draw(t_profile,red);

// liquid sublayer
//path liquid_sublayer=(x_1_1,0)--(x_1_1-dx,dx/4)--(x_1_1-dx,0)--cycle;
//fill(liquid_sublayer,green);
//path liquid_sublayer2=(x_1_3,0)--(x_1_3+dx,dx/4)--(x_1_3+dx,0)--cycle;
//fill(liquid_sublayer2,green);

// Wall with Cavities
//Label T=Label("$T_l$",position=BeginPoint);
//draw((0,0)--(x_1_1,0)--(x_1_2,-dh)--(x_1_3,0)--(l_w,0));
//// Bubble
//path bubble=(x_1_1,0)--(x_1_1-dx,dx/4){-dx,dx/4}..(x_1_2,2.){right}..{-dx,-dx/4}(x_1_3+dx,dx/4)--(x_1_3,0);
//draw(bubble,blue);
//
//label("Liquid Sublayer",(x_1_3+2,0.5),align=E);
//draw((x_1_3+2,0.5)--(x_1_3+dx,0.1),arrow=Arrow);
//
//// Saturation Line
//// draw((3.5,0)--(3.5,2),dashed);
//// label("$T_{sat}$",(3.5,0),align=S);
















