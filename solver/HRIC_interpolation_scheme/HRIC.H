/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2011-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::HRIC

Description
    Class with limiter function which returns the limiter for the
    HRIC differencing scheme based on phict obtained from the LimiterFunc
    class.

    Used in conjunction with the template class LimitedScheme.

SourceFiles
    HRIC.C

\*---------------------------------------------------------------------------*/

#ifndef HRIC_H
#define HRIC_H

#include "vector.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class HRICLimiter Declaration
\*---------------------------------------------------------------------------*/

template<class LimiterFunc>
class HRICLimiter
:
    public LimiterFunc
{

public:

    HRICLimiter(Istream&)
    {}

    scalar limiter
    (
        const scalar,
        const scalar faceFlux,
	const scalar Cof,
        const typename LimiterFunc::phiType& phiP,
        const typename LimiterFunc::phiType& phiN,
        const typename LimiterFunc::gradPhiType& gradcP,
        const typename LimiterFunc::gradPhiType& gradcN,
        const vector& d
    ) const
    {
        scalar phict = LimiterFunc::phict
        (
            faceFlux, phiP, phiN, gradcP, gradcN, d
        );
	scalar phift,costheta,gamma;
	if (phict<0. || phict>1.)
	{
		phift=phict;
	}
	else if (phict<=0.5)
	{
		phift=2.*phict;
	}
	else
	{
		phift=1.;
	}
	if (faceFlux>0)
	{
		costheta = mag((gradcP & d)/(mag(gradcP)*mag(d) + SMALL));
	}
	else
	{
		costheta = mag((gradcN & d)/(mag(gradcN)*mag(d) + SMALL));
	}
	gamma=sqrt(costheta);
	phift=gamma*phift+(1.-gamma)*phict;
	if (Cof<0.3)
	{
		phift=phift;
	}
	else if (Cof>0.7)
	{
		phift=phict;
	}
	else
	{
		phift=phict+(phift-phict)*(0.7-Cof)/(0.7-0.3);
	}

        return (phift-phict)/(1-phict);
    }
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
