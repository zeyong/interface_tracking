   Info<< "Setting velocity field\n"<<endl;
   const fvMesh& mesh = U.mesh(); 
   scalar t_pi=3.1415926535897932384;
   scalar t_mid=2.5*t_pi;

   forAll(U, i)
   {
       const scalar x = mesh.C()[i][0];
       const scalar y = mesh.C()[i][1];
       if (U.mesh().time().value()<t_mid)
       { 
	       U[i][0] = Foam::cos(x-t_pi/2)*Foam::sin(y-t_pi/2);
	       U[i][1] = -Foam::sin(x-t_pi/2)*Foam::cos(y-t_pi/2);
       }
       else
       {
	       U[i][0] = -Foam::cos(x-t_pi/2)*Foam::sin(y-t_pi/2);
	       U[i][1] = Foam::sin(x-t_pi/2)*Foam::cos(y-t_pi/2);

       }
   }
   U.correctBoundaryConditions();

   Info<< "Setting flux\n"<<endl;
   phi=fvc::flux(U); 
